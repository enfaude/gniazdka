package tb.sockets.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Konsola {

	public static void main(String[] args) {
		try {
			Socket sock = new Socket("172.16.90.183", 6666);
			DataOutputStream so = new DataOutputStream(sock.getOutputStream());
			//writeBytes nie zostawia spacji miedzy znakami przy wypisywaniu tekstu na serwerze
			so.writeBytes("pomyslna wymiana danych z kolega ze stanowiska obok");
			so.close();
			sock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
